//********************************************************************************
/*!
\author     Kraemer E.
\date       04.10.2020

\file       OS_RealTimeClock.c
\brief      Real time clock handling

***********************************************************************************/

/********************************* includes **********************************/
#include "OS_RealTimeClock.h"

#ifdef USE_OS_REAL_TIME_CLOCK
#include "OS_ErrorDebouncer.h"
#include "HAL_RTC.h"

/********************************* Defines **********************************/

/************************* local function prototypes *************************/
static bool CheckRTCResult(teSysTimerReturn eTimerReturn);

/************************* local data (const and var) ************************/

/************************ export data (const and var) ************************/

/****************************************** local functions *********************************************/
//********************************************************************************
/*!
\author     Kraemer E.
\date       20.01.2019
\brief      Compares the result value from the HAL access and put either a fault into
            the error debouncer or do nothing and return a success.
\return     bReturnVal - True when timer operation was successful.
\param      eTimerReturn - The return value of the timer access.
***********************************************************************************/
static bool CheckRTCResult(teSysTimerReturn eTimerReturn)
{
    bool bReturnVal = false;

    switch (eTimerReturn)
    {
        case eSysTimer_Success:             bReturnVal = true;                                         break;
        case eSysTimer_Invalid:             OS_ErrorDebouncer_PutErrorInQueue(eFlashCRCInvalid);       break;
        case eSysTimer_InvalidTimer:        OS_ErrorDebouncer_PutErrorInQueue(eTimerInvalidTimer);     break;
        default:                            OS_ErrorDebouncer_PutErrorInQueue(eUnknownReturnValue);    break;
    }

    return bReturnVal;
}


/****************************************** External visible functions **********************************/
//********************************************************************************
/*!
\author  Kraemer E
\date    04.10.2020
\brief   Initializes the RTC and checks for correct return value of the operation.
\param   none
\return  bReturn - True when access was correct.
***********************************************************************************/
bool OS_RealTimeClock_Init(void)
{
    /* Initialize the RTC */
    teSysTimerReturn eTimerReturn = HAL_RealTimeClock_Init();

    bool bReturn = CheckRTCResult(eTimerReturn);

    return bReturn;
}


//********************************************************************************
/*!
\author  Kraemer E
\date    04.10.2020
\brief   Just a wrapper function
\param   pucHour - Pointer where the current hour shall be saved
\param   pucMin - Pointer where the current minutes shall be saved
\param   pulTicks - Pointer where the current unix-tick shall be saved
\return  none
***********************************************************************************/
void OS_RealTimeClock_GetClock(u8* pucHour, u8* pucMin, u32* pulTicks)
{
    HAL_RealTimeClock_GetClock(pucHour, pucMin, pulTicks);
}


//********************************************************************************
/*!
\author  Kraemer E
\date    06.10.2020
\brief   Sets actual epoch time
\param   ulEpochTick - The epoch/unix time
\return  none
***********************************************************************************/
void OS_RealTimeClock_SetTime(u32 ulEpochTick)
{
    HAL_RealTimeClock_SetTime(ulEpochTick);
}

//********************************************************************************
/*!
\author  Kraemer E
\date    06.10.2020
\brief   Gets the status of the day-save-time
\param   none
\return  bool - True when DST is active otherwise false
***********************************************************************************/
bool OS_RealTimeClock_GetDaySaveTime(void)
{
    return HAL_RealTimeClock_GetDaySaveTimeStatus();
}

#endif //USE_OS_REAL_TIME_CLOCK