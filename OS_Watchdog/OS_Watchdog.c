/*
 * OS_Watchdog.c
 *
 *  Created on: 29.04.2021
 *      Author: kraemere
 */


/********************************* includes **********************************/
#include "OS_Watchdog.h"

#if USE_OS_WATCHDOG
    
#include "OS_ErrorDebouncer.h"
#include "OS_Communication.h"
/***************************** defines / macros ******************************/

/************************ local data type definitions ************************/

typedef enum
{
    eWdtAction_WdtInit,
    eWdtAction_SysTimerInit,
    eWdtAction_SysTimerEnableDisable,
    eWdtAction_SysTimerResetCnt,
    eWdtAction_SysTimerSetCallback
}teWdtAction;

/************************* local function prototypes *************************/

/************************* local data (const and var) ************************/

/************************ export data (const and var) ************************/


/****************************** local functions ******************************/

//***************************************************************************
/*!
\author     KraemerE
\date       29.04.2021
\brief      Checks the timer return value and handles them.
\return     none
\param      eReturn - The return value of a HAL function.
******************************************************************************/
void CheckTimerReturnValue(teSysTimerReturn eReturn, teWdtAction eTimerAction)
{
    switch(eReturn)
    {
        case eSysTimer_Invalid:
        case eSysTimer_InvalidTimer:
        {
            OS_ErrorDebouncer_PutErrorInQueue(eTimerInvalidTimer);
            break;
        }

        case eSysTimer_LimitReached:
        {
            OS_Communication_SendDebugMessage("SysLimit");
            break;
        }
        case eSysTimer_TimerAlreadyDisabled:
        {
            OS_Communication_SendDebugMessage("TDisabled");
            break;
        }

        case eSysTimer_TimerAlreadyEnabled:
        {
            OS_Communication_SendDebugMessage("TEnabled");
            break;
        }

        case eSysTimer_TimerIsDisabled:
        {
            #ifdef VERBOSE
                #warning TODO: Send debug message
            #endif
            break;
        }

        case eSysTimer_Success:
        {
            OS_Communication_SendDebugMessage("TSuccess");
            break;
        }
        default:
            break;
    }
}
/************************ externally visible functions ***********************/

//***************************************************************************
/*!
\author     KraemerE
\date       29.04.2021
\brief      Initializes the WDT with a given interval
\return     none
\param      uiResetInterval - The desired reset interval in milliseconds
******************************************************************************/
void OS_WDT_InitWatchdog(u16 uiResetInterval)
{
    teSysTimerReturn eReturn = HAL_WDT_InitWatchdog(uiResetInterval);
    CheckTimerReturnValue(eReturn, eWdtAction_WdtInit);
}


//***************************************************************************
/*!
\author     KraemerE
\date       29.04.2021
\brief      Clears the WDT counter
\return     none
\param      none
******************************************************************************/
void OS_WDT_ClearWatchdogCounter(void)
{
    HAL_WDT_ClearWatchdogCounter();
}


//***************************************************************************
/*!
\author     KraemerE
\date       29.04.2021
\brief      Initializes a system timer (WDT-Timer) with the given interval.
\return     none
\param      eSystemTimer - Timer which shall be initalized
\param      uiTimerValue - Timer value in milliseconds
******************************************************************************/
void OS_WDT_SystemTimerInit(teSystemTimers eSystemTimer, u16 uiTimerValue)
{
    teSysTimerReturn eReturn = HAL_WDT_SystemTimerInit(eSystemTimer, uiTimerValue);
    CheckTimerReturnValue(eReturn, eWdtAction_SysTimerInit);
}


//***************************************************************************
/*!
\author     KraemerE
\date       29.04.2021
\brief      Enables or disables a specific system timer (WDT-Timer)
\return     none
\param      eSystemTimer - Timer which shall be enabled or disabled
\param      bEnableDisable - True to enable the timer and false for disable.
******************************************************************************/
void OS_WDT_SystemTimerDisableEnable(teSystemTimers eSystemTimer, bool bEnableDisable)
{
    teSysTimerReturn eReturn = HAL_WDT_SystemTimerDisableEnable(eSystemTimer, bEnableDisable);
    CheckTimerReturnValue(eReturn, eWdtAction_SysTimerEnableDisable);
}


//***************************************************************************
/*!
\author     KraemerE
\date       29.04.2021
\brief      Clears the System timer counter
\return     none
\param      eSystemTimer - Timer of which the counter shall be cleared.
******************************************************************************/
void OS_WDT_SystemTimerResetCounter(teSystemTimers eSystemTimer)
{
    teSysTimerReturn eReturn = HAL_WDT_SystemTimerResetCounter(eSystemTimer);
    CheckTimerReturnValue(eReturn, eWdtAction_SysTimerResetCnt);
}


//***************************************************************************
/*!
\author     KraemerE
\date       29.04.2021
\brief      Sets a callback function on a specific system timer.
\return     none
\param      eSystemTimer - Timer which shall be a callback set
\param      pCallbackFunction - The function which shall be called when the
                                system timer has a match on compare.
******************************************************************************/
void OS_WDT_SystemTimerSetCallbackFunction(teSystemTimers eSystemTimer, void* pCallbackFunction)
{
    teSysTimerReturn eReturn = HAL_WDT_SystemTimerSetCallbackFunction(eSystemTimer, pCallbackFunction);
    CheckTimerReturnValue(eReturn, eWdtAction_SysTimerSetCallback);
}


//***************************************************************************
/*!
\author     KraemerE
\date       29.04.2021
\brief      Initializes the system timer interrupt module. Interrupt module
            is used for all WDT-System-Timers.
\return     none
\param      none
******************************************************************************/
void OS_WDT_SystemTimerInterruptInit(void)
{
    HAL_WDT_SystemTimerInterruptInit();
}


//***************************************************************************
/*!
\author     KraemerE
\date       29.04.2021
\brief      Enables or disables the system timer interrupt module. Interrupt module
            is used for all WDT-System-Timers.
\return     none
\param      none
******************************************************************************/
void OS_WDT_SystemTimerInterruptEnableDisable(bool bEnableDisable)
{
    HAL_WDT_SystemTimerInterruptEnableDisable(bEnableDisable);
}

#endif //USE_OS_WATCHDOG